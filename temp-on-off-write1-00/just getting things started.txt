Progress:

Got a one wire reading and reporting in.
Printing Address
working on simplifying read from temp sensor so can figure out easiest way to work with them.
Have moved alot of the work for reading a sensor into another function so that becomes easier when reading multiple sensors (will only have to write one line to read from a sensor.)
updated so that sending "Address" will print device addresses
also now has a way to store adresses so can load up multiple sensors and remember their adresses.
this means that wiring all the one wire sensors on the one pin is handy, as long as we don't need high speed readings.(or heaps of sensors, it's about 1 seconds per sensor)

To Use in current state need to install the one wire library using the arduino Manage librarys system:
///////////////////////
Open up the arduino program.
Sketch -> Include Library
then at the top of the list is Manage Libraries.
type one wire into the filter box at the top right select the library "MAX31850 BallasTemp" and install that.
once it's done that your code should compile.
///////////////////////

Currently Changes are made by changing code
These are the bits you'll want to adjust.

int setLowTemp[4] = {31,32,33,34}; //these two arrays are the temperature targets high and low
int setHighTemp[4] = {30,31,32,33};
int relayPins[4] = {4,5,6,7}; //the list of relay pins.

the variables are all matched up in order. so for example. Sensor 1 has a setLowTemp of 31, a setHighTemp of 30 and is matched with relays on pin 4.
to see the sensor orders type "address" into the serial monitor and hit send. you should then get a list of all the sensors in order.
