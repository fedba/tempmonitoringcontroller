#include <OneWire.h>

#define E1 13 //this will be for seting up relay pins

byte addr[8];
byte addressStorage[8][10]; //Array for storing temp sensor addresses in.

int setLowTemp[4] = {45,50,55,90}; //these two arrays are the temperature targets high and low
int setHighTemp[4] = {46,51,56,91};

int latestTemp[10]; //stores the latest reading for each temp sensors

int relayPins[4] = {4,5,6,7}; //the list of relay pins.

int numberOfTemp = 10; //for counting how many sensors there are.

int count = 0; // initialize to zero to make sure first thing arduino does is scan for sensors.
int serialDataAvailable =0;
String receivedString;

// DS18S20 Temperature chip i/o
OneWire ds(10);  // on pin 10

///////////////////////////////////
// setup
//////////////////////////////////

void setup(void) {
  // initialize inputs/outputs
  // start serial port
  Serial.begin(9600);

  for (int i = 0; i< 4; i++) // This loop goes through the relays and sets them as OUTPUT and makes sure they are set LOW.
  {
    pinMode(relayPins[i], OUTPUT);
    digitalWrite(relayPins[i], LOW);
  }

}


///////////////////////////////////
// the loop
//////////////////////////////////


void loop(void) {


//THis section is for checking messages from the computer(serial port)

  serialDataAvailable = Serial.available();
  if(serialDataAvailable){
    receivedString = Serial.readString();
    if (receivedString.startsWith("Address"))
    {
      count = 0;
      numberOfTemp = 10;
    }
  }


//this loop scans for temperature sensors(up to 10 sensors).
//it will count how many temerature sensors there are and store their addresses in addressStorage
  while (count < numberOfTemp)
  {
    if ( !ds.search(addr)) {
      Serial.print("there are ");
      numberOfTemp = count;
      Serial.print(numberOfTemp);
      Serial.println(" temp sensors connected");
      ds.reset_search();
    }else{

      Serial.print("Address for sensor number ");
      Serial.print(count);
      storeAddress(count,addr);
      getAddress(count,addr);
      for (int i = 0; i<= 7; i++)
      {
        Serial.print(addr[i]);
      }
      Serial.println(" ");
      count++;
    }
  }

//for loop reads the temperature for each temp sensor and writes to serial port
  for (int i = 0; i< numberOfTemp; i++)
  {
    Serial.print("Sensor ");
    Serial.print(i);
    Serial.print("reading :");
    latestTemp[i] = readTemperature(addr);
  Serial.println(latestTemp[i]);
  }

  //this for loop turing on/off relays if they are at right conditions

  for (int i = 0; i< numberOfTemp; i++)
  {
    if (latestTemp[i] < setHighTemp[i])
    {
      digitalWrite(relayPins[i],HIGH);
    }

    if (latestTemp[i] > setLowTemp[i])
    {
      digitalWrite(relayPins[i],LOW);
    }


  }
}


///////////////////////////////////
// other functions
//////////////////////////////////

void getAddress(int desiredAddress, byte *singleAddress)
{
  for (int i = 0; i<= 7; i++)
  {
    singleAddress[i] = addressStorage[i][desiredAddress];
  }
}


void storeAddress(int desiredAddress, byte *singleAddress)
{
  for (int i = 0; i<= 7; i++)
  {
    addressStorage[i][desiredAddress] = singleAddress[i];
  }
}

float readTemperature(byte *address)
{
  int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;
  byte present = 0;
  byte data[12];

  ds.reset();
  ds.select(address);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(address);
  ds.write(0xBE);         // Read Scratchpad

  for ( int i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }

  LowByte = data[0];
  HighByte = data[1];
  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }
  Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

  Whole = Tc_100 / 100;  // separate off the whole and fractional portions
  Fract = Tc_100 % 100;
  float tempTest = Whole + (Fract*0.01);
  if (SignBit) // If its negative
  {
    tempTest = 0 - tempTest;
  }
  return tempTest;
}
