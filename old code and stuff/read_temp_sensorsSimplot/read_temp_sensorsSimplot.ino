#include <OneWire.h>
#include <math.h>


class Plotting {
public:
  
  //Plots a Number of Floats
  static void plot(float dataF1, int scale) {
    int dataI1 = (int) (scale*dataF1);
    Plotting::plot(dataI1);
  }
  static void plot(float dataF1, float dataF2, int scale) {
    int dataI1 = (int) (scale*dataF1);
    int dataI2 = (int) (scale*dataF2);
    Plotting::plot(dataI1, dataI2);
  }
  static void plot(float dataF1, float dataF2, float dataF3, int scale) {
    int dataI1 = (int) (scale*dataF1);
    int dataI2 = (int) (scale*dataF2);
    int dataI3 = (int) (scale*dataF3);
    Plotting::plot(dataI1, dataI2, dataI3);
  }
  static void plot(float dataF1, float dataF2, float dataF3, float dataF4, int scale) {
    int dataI1 = (int) (scale*dataF1);
    int dataI2 = (int) (scale*dataF2);
    int dataI3 = (int) (scale*dataF3);
    int dataI4 = (int) (scale,dataF4);
    Plotting::plot(dataI1, dataI2, dataI3, dataI4);
  }
  //Plots a number of Ints
  static void plot(int data1, int data2, int data3, int data4) {
    int buffer[20];
    int pktSize;
    buffer[0] = 0xCDAB;
    buffer[1] = 4*sizeof(int);
    buffer[2] = data1;
    buffer[3] = data2;
    buffer[4] = data3;
    buffer[5] = data4;
    pktSize = 2 + 2 + (4*sizeof(int));
    Serial.write((uint8_t * )buffer, pktSize);
  }
  static void plot(int data1, int data2, int data3) {
    int buffer[20];
    int pktSize;
    buffer[0] = 0xCDAB;
    buffer[1] = 3*sizeof(int);
    buffer[2] = data1;
    buffer[3] = data2;
    buffer[4] = data3;
    pktSize = 2 + 2 + (3*sizeof(int));
    Serial.write((uint8_t * )buffer, pktSize);
  }
  static void plot(int data1, int data2) {
    int buffer[20];
    int pktSize;
    buffer[0] = 0xCDAB;
    buffer[1] = 2*sizeof(int);
    buffer[2] = data1;
    buffer[3] = data2;
    pktSize = 2 + 2 + (2*sizeof(int));
    Serial.write((uint8_t * )buffer, pktSize);
  }
  static void plot(int data1) {
    int buffer[20];
    int pktSize;
    buffer[0] = 0xCDAB;
    buffer[1] = 1*sizeof(int);
    buffer[2] = data1;
    pktSize = 2 + 2 + (1*sizeof(int));
    Serial.write((uint8_t * )buffer, pktSize);
  }
  
};

  int sensorCount = 0;
  int sensors[4];

// DS18S20 Temperature chip i/o
OneWire ds(10);  // on pin 10

void setup(void) {
  // initialize inputs/outputs
  // start serial port
  Serial.begin(9600);
  
}

void loop(void) {
  
  int HighByte, LowByte, TReading, SignBit, Tc_100, Whole, Fract;
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  if ( !ds.search(addr)) {
      sensorCount=0;
      Plotting::plot(sensors[0],sensors[1],sensors[2],sensors[3]);
      ds.reset_search();
      return;
  }


  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(3000);     // maybe 750ms is enough, maybe not //###############################################################################################
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }

  
  
  LowByte = data[0];
  HighByte = data[1];
  TReading = (HighByte << 8) + LowByte;
  SignBit = TReading & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    TReading = (TReading ^ 0xffff) + 1; // 2's comp
  }
  Tc_100 = (6 * TReading) + TReading / 4;    // multiply by (100 * 0.0625) or 6.25

  Whole = Tc_100 / 100;  // separate off the whole and fractional portions
  Fract = Tc_100 % 100;

  sensorCount++;
  //if (SignBit) // If its negative
  //{
  //   Serial.print("-");
  //}
  //Serial.print(Whole);
  
  sensors[sensorCount] = Whole;
  //Serial.print(".");
  //if (Fract < 10)
  //{
  //   Serial.print("0");
  //}
  //Serial.print(Fract);

  //Serial.print(",");
}
